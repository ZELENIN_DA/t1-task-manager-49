package ru.t1.dzelenin.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractModel {

    @Nullable
    private String userId;

    @NotNull
    private Date date = new Date();

    @Nullable
    private Role role = null;

}
