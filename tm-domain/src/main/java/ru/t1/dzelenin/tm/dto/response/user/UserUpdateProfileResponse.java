package ru.t1.dzelenin.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.model.UserDTO;
import ru.t1.dzelenin.tm.dto.response.AbstractUserResponse;

@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
