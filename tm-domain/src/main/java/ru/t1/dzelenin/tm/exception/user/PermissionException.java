package ru.t1.dzelenin.tm.exception.user;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}

