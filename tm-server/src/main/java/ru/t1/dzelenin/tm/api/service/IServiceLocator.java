package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.service.dto.IProjectDTOService;
import ru.t1.dzelenin.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.dzelenin.tm.api.service.dto.ITaskDTOService;
import ru.t1.dzelenin.tm.api.service.dto.IUserDTOService;

public interface IServiceLocator {

    @NotNull
    IProjectDTOService getProjectService();

    @NotNull
    ITaskDTOService getTaskService();

    @NotNull
    IProjectTaskDTOService getProjectTaskService();

    @NotNull
    IUserDTOService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

}