package ru.t1.dzelenin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.dzelenin.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.dzelenin.tm.enumerated.Status;

import java.util.Date;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IUserOwnedDTORepository<M>, IDTOService<M> {

    @Nullable
    M create(@Nullable String userId, @Nullable String name);

    @Nullable
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @Nullable
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}