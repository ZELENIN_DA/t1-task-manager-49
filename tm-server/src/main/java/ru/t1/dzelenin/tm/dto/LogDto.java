package ru.t1.dzelenin.tm.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.UUID;

@Getter
@XmlRootElement
public class LogDto implements Serializable {

    @NotNull
    private final String className;

    @NotNull
    private final String date;

    @NotNull
    private final String entity;

    @NotNull
    private final String id = UUID.randomUUID().toString();

    @NotNull
    private final String type;

    public LogDto(@NotNull final String className,
                  @NotNull final String date,
                  @NotNull final String type,
                  @NotNull final String entity
    ) {
        this.className = className;
        this.date = date;
        this.type = type;
        this.entity = entity;
    }

}

