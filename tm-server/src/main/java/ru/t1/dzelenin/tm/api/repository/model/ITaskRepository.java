package ru.t1.dzelenin.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.dto.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeTasksByProjectId(@NotNull String userId, @NotNull String projectId);

}
