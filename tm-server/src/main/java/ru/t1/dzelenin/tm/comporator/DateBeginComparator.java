package ru.t1.dzelenin.tm.comporator;

import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.model.IHasDateBegin;

import java.util.Comparator;

public enum DateBeginComparator implements Comparator<IHasDateBegin> {

    INSTANCE;

    @Override
    public int compare(@Nullable final IHasDateBegin o1, @Nullable final IHasDateBegin o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getDateBegin() == null || o2.getDateBegin() == null) return 0;
        return o1.getDateBegin().compareTo(o2.getDateBegin());
    }

}