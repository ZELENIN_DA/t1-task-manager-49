package ru.t1.dzelenin.tm.command.task;

import ru.t1.dzelenin.tm.dto.model.TaskDTO;

import java.util.List;

public abstract class AbstractTaskListCommand extends AbstractTaskCommand {

    protected void renderTasks(final List<TaskDTO> tasks) {
        int index = 1;
        for (TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
    }

}
