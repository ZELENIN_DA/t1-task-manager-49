package ru.t1.dzelenin.tm.command.data;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.domain.DataJsonSaveJaxBRequest;
import ru.t1.dzelenin.tm.enumerated.Role;


public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @Getter
    @NotNull
    private final String name = "data-save-json-jaxb";

    @Getter
    @NotNull
    private final String description = "Save data in json file.";

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        getDomainEndpoint().saveDataJsonJaxb(new DataJsonSaveJaxBRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}



