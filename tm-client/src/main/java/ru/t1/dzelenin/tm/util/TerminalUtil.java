package ru.t1.dzelenin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.exception.field.NumberIncorrectException;

import java.util.Date;
import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @Nullable
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @Nullable final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (final Exception e) {
            throw new NumberIncorrectException(value, e);
        }
    }

    @Nullable
    static Date nextDate() {
        final String value = nextLine();
        return DateUtil.toDate(value);
    }

}
