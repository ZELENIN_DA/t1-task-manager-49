package ru.t1.dzelenin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.component.Bootstrap;

public final class Application {

    public static void main(@Nullable final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run();
    }

}